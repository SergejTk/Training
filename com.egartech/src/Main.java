
import model.Product;
import dao.ProductDAOimpl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException{

        List<Product> products = new ArrayList<Product>();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            products.add(new Product("meat", 2.34f, 5, 3.67f, format.parse("10-02-2018")));
            products.add(new Product("nuts", 2.65f, 3, 4.74f, format.parse("13-03-2018")));
            products.add(new Product("fish", 7.34f, 2, 4.99f, format.parse("05-04-2018")));
        }catch (ParseException e){
            System.out.println(e.getMessage());
        }

        ProductDAOimpl p = new ProductDAOimpl();
        for(Product product : products){
            p.addProduct(product);
        }
        p.getListProducts();
        try {
            p.updateProduct(new Product(2, "chicken", 1.34f, 5, 0.44f, format.parse("31-05-2018")));
        }catch (ParseException e){
            System.out.println(e.getMessage());
        }
        p.getListProducts();
        p.deleteProduct("meat");
        p.getListProducts();
    }
}
