package dao;

import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAOimpl implements ProductDAO {

    @Override
    public Product getProduct(String name) {
        Product product = null;
        try (Connection conn = dbconnection(); PreparedStatement prstm = conn.prepareStatement("SELECT * FROM products WHERE name = ?;");){
            prstm.setString(1, name);
            ResultSet rs = prstm.executeQuery();
            product = new Product(rs.getLong(1), rs.getString(2), rs.getFloat(3), rs.getInt(4), rs.getFloat(5), rs.getDate(6));

        } catch (SQLException e){
            e.getMessage();
        }
        return product;
    }

    @Override
    public void addProduct(Product product) {
        try (Connection conn = dbconnection(); PreparedStatement prstm = conn.prepareStatement("INSERT INTO products VALUES ( ?, ?, ?, ?, ?)");){
            prstm.setString(1, product.getName());
            prstm.setFloat(2, product.getCost());
            prstm.setInt(3, product.getAmount());
            prstm.setFloat(4, product.getCostDelivery());
            prstm.setDate(5, (Date) product.getDate());
            int i = prstm.executeUpdate();
            if (i != 0) {
                System.out.println("Inserted");
            } else {
                System.out.println("not Inserted");
            }
        } catch (SQLException e){
            e.getMessage();
        }

    }

    @Override
    public void updateProduct(Product product) {
        try (Connection conn = dbconnection(); PreparedStatement prstm = conn.prepareStatement("UPDATE products SET name = ?, cost = ?, amount = ?, costDelivery = ?, date = ? WHERE id = ?");){
            prstm.setString(1, product.getName());
            prstm.setFloat(2, product.getCost());
            prstm.setInt(3, product.getAmount());
            prstm.setFloat(4, product.getCostDelivery());
            prstm.setDate(5, (Date) product.getDate());
            prstm.setLong(6, product.getId());
            int i = prstm.executeUpdate();
            if (i != 0) {
                System.out.println("updated");
            } else {
                System.out.println("not updated");
            }
        } catch (SQLException e){
            e.getMessage();
        }
    }

    @Override
    public void deleteProduct(String name) {
        try (Connection conn = dbconnection(); PreparedStatement prstm = conn.prepareStatement("DELETE FROM products WHERE name = ?");){
            prstm.setString(1, name);
            int i = prstm.executeUpdate();
            if (i != 0) {
                System.out.println("deleted");
            } else {
                System.out.println("not deleted");
            }
        } catch (SQLException e){
            e.getMessage();
        }
    }

    @Override
    public List<Product> getListProducts() {
        List<Product> products = new ArrayList<>();
        try (Connection conn = dbconnection(); PreparedStatement prstm = conn.prepareStatement("SELECT * FROM products ORDER BY id");){
            prstm.executeQuery();
        } catch (SQLException e){
            e.getMessage();
        }
        return products;
    }

    private Connection dbconnection() {
        try {
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.getStackTrace();
        }
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/Product", "user", "password");
        } catch (SQLException e) {
            System.out.println("Connection error");
        }
        return conn;
    }
}
