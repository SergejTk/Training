package dao;


import model.Product;

import java.util.List;

public interface ProductDAO {
    Product getProduct(String name);
    void addProduct(Product product);
    void updateProduct(Product product);
    void deleteProduct(String name);
    List<Product> getListProducts();
}
