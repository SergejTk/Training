package model;

import java.util.Date;

public class Product {

    private  long id;

    private String name;

    private float cost;

    private int amount;

    private float costDelivery;

    private Date date;

    public Product() {
    }

    public Product(String name, float cost, int amount, float costDelivery, Date date) {
        this.name = name;
        this.cost = cost;
        this.amount = amount;
        this.costDelivery = costDelivery;
        this.date = date;
    }

    public Product(long id, String name, float cost, int amount, float costDelivery, Date date) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.amount = amount;
        this.costDelivery = costDelivery;
        this.date = date;
}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getCostDelivery() {
        return costDelivery;
    }

    public void setCostDelivery(float costDelivery) {
        this.costDelivery = costDelivery;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
